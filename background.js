
chrome.runtime.onMessage.addListener(
  function (arg, sender, sendResponse) {
    
    var args = arg.collection;
        for (i in args) {
            var img_url = args[i];
          try {
            
                saveas = img_url.href.replace(/[^a-zA-Z0-9]/g, '-');
            }
            catch (problem) {
              console.log(problem)
            }
          
            chrome.downloads.download({
                url: img_url,
                
                saveAs: false
            });
        }
    });

function sendResponse() {
}

chrome.runtime.onInstalled.addListener(function () {
  chrome.declarativeContent.onPageChanged.removeRules(undefined, function () {
    chrome.declarativeContent.onPageChanged.addRules([{
      conditions: [new chrome.declarativeContent.PageStateMatcher({
        pageUrl: { hostEquals: 'cursos.alura.com.br' },
      })
      ],
      actions: [new chrome.declarativeContent.ShowPageAction()]
    }]);
  });
});
