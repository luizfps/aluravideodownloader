  

  elements = []; 
  contador = 0;
  var filedownloads = async function (files) {
    
    files = files.map((item) => item.href.toString());
   
    var param = { collection: files };
      chrome.runtime.sendMessage(param);
     
  }
 
    async function f() {
      
      arvore = "";
      
      sectionLink = document.querySelectorAll(".task-menu-sections-select > option");

      for (i = 0; i < sectionLink.length; i++) {
        var videosPorSecao = [];
        await new Promise(function (resolve, reject) {
          var titulo = document.getElementsByClassName('task-menu-header-info-title');

          titulo = titulo[0].href + '/section/' + sectionLink[i].value;

          var xhttp = new XMLHttpRequest();
          xhttp.onreadystatechange = async function () {
            if (this.readyState == 4 && this.status == 200) {
              var sections = "|---- seções: " + sectionLink[i].innerHTML;
              sendMessageToPopup((sectionLink[i].innerHTML));

              //console.log(sections);
              var documento = this.responseXML;

              var videosmenu = documento.getElementsByClassName("task-menu-nav-item-link task-menu-nav-item-link-VIDEO");

              for (j = 0; j < videosmenu.length; j++) {
                await new Promise((resolve2, reject) => {
                  //console.log("|  |-- Aula:", videosmenu[j].children[2].children[0].innerHTML.trim());
                  var xhttp2 = new XMLHttpRequest();
                  xhttp2.onreadystatechange = function () {
                    if (this.readyState == 4 && this.status == 200) {
                      //console.log(this.responseXML);
                      var videodocumento = this.responseXML;

                      var videocontainerdata = videodocumento.getElementById("video-container-data");
                      var e = videocontainerdata.dataset.course;
                      var a = videocontainerdata.dataset.taskId;
                      //console.log(a + "  " + e);

                      var xhttp3 = new XMLHttpRequest();
                      xhttp3.onreadystatechange = function () {
                        if (this.readyState == 4 && this.status == 200) {

                          var link = JSON.parse(this.response);
                          if (typeof link[0] !== typeof undefined) {
                            var hdlink = link[0].link;
                          }
                          if (typeof link[1] !== typeof undefined) {
                            var sdlink = link[1].link;
                          }
                          
                          //console.log(hdlink);
                          var element = document.createElement('a');
                          element.setAttribute('href', hdlink);
                          var filename = videodocumento.getElementsByClassName("task-body-header-title-text")[0].innerHTML;
                          element.setAttribute('download', filename);
                          elements.push(element);
                          document.body.appendChild(element);
                          //element.style.display = 'none';
                        
                          //console.log("|     |-- Aula:", videosmenu[j].children[2].children[0].innerHTML.trim() + " => " + hdlink);
                          contador++;
                          videosPorSecao.push({descricao:videosmenu[j].children[2].children[0].innerHTML.trim(),link:hdlink,total:contador});
                          resolve2("");
                        }
                      }
                      xhttp3.open("GET", "/course/" + e + "/task/" + a + "/video");
                      xhttp3.overrideMimeType = 'video / mp4';
                      xhttp3.send();
                    }
                  }
                  xhttp2.open("GET", videosmenu[j].href);
                  xhttp2.responseType = "document";
                  xhttp2.send();
                }).then();
              }
              resolve("");
            };
          }

          xhttp.open("GET", titulo);
          xhttp.responseType = "document";
          xhttp.send();

        }).then();
        sendMessageToPopup(videosPorSecao);
      }
      console.log(arvore);
      console.log("Total de aulas: ", contador);
    }
  
  

chrome.runtime.onMessage.addListener(
  function(request, sender, sendResponse) {
    if(request.greeting =="procurarvideos"){
      console.log("recebido uma requisicao vinda do popup");
         contador = 0;
        f();
      }
      return true;
    }
  );

function sendMessageToPopup(param){
  chrome.runtime.sendMessage({greeting:param});
}